/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 11:31:19 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/02 23:02:10 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <iostream>

int			main(int ac, char **av)
{
	int		i(1), j;
	char	c;

	if (ac == 1)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
		return (0);
	}
	while (i < ac)
	{
		j = 0;
		while (av[i][j])
		{
			if (av[i][j] > 96 && av[i][j] < 123)
				c = av[i][j] - 32;
			else
				c = av[i][j];
			std::cout << c;
			j++;
		}
		if (av[i + 1])
			std::cout << ' ';
		i++;
	}
	std::cout << std::endl;
	return (0);
}
