/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phonebook.class.hpp                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/02 13:27:53 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 13:40:53 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHONEBOOK_CLASS_HPP
# define PHONEBOOK_CLASS_HPP

# include <iostream>
# include <iomanip>
# include <string>
# include "colors.h"

# define BAD_CMD	(cmd != "exit" && cmd != "add" && cmd != "search")
# define EXIT		(cmd == "exit")
# define ADD		(cmd == "add")
# define SEARCH		(cmd == "search")

class Sample
{
public:

       std::string  firstName;
       std::string	lastName;
       std::string	nickname;
       std::string	login;
       std::string	address;
       std::string	email;
       std::string	phone;
       std::string	birthday;
       std::string	meal;
       std::string	secret;
	
       Sample( void );
       ~Sample( void );
};

#endif
