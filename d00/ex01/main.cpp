/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 12:56:51 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 13:42:06 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "phonebook.class.hpp"
#include <cstdlib>

static void		error(std::string cmd)
{
	std::cout << BOLD_RED << "Phonebook: illegal command - " << END;
	std::cout << BOLD_BROWN << cmd << END << std::endl;
	std::cout << BOLD_RED << "usage: " << END;
	std::cout << GREEN << "[add | search | exit]" << std::endl << END;
	cmd.clear();
}

static void		prompt(void)
{
	std::cout << CYAN;
	std::cout << "Phonebook " << END;
	std::cout << MAGENTA << "$> " << END;
}

static void     adder(Sample &contact)
{
    std::cout << "First Name: ";
    std::getline(std::cin, contact.firstName);
    std::cout << "Last Name: ";
    std::getline(std::cin, contact.lastName);
    std::cout << "NickName: ";
    std::getline(std::cin, contact.nickname);
    std::cout << "Login: ";
    std::getline(std::cin, contact.login);
    std::cout << "Postal address: ";
    std::getline(std::cin, contact.address);
    std::cout << "Email address: ";
    std::getline(std::cin, contact.email);
    std::cout << "Phone number: ";
    std::getline(std::cin, contact.phone);
    std::cout << "Birthday date: ";
    std::getline(std::cin, contact.birthday);
    std::cout << "Favourite meal: ";
    std::getline(std::cin, contact.meal) ;
    std::cout << "Darkest secret: ";
    std::getline(std::cin, contact.secret);
}

static void     disp(std::string str)
{
    int     len(str.length()), i=0;

    if (len > 10)
    {
        while (i < 9)
        {
            std::cout << str[i];
            i++;
        }
       std::cout <<  ".|";
    }
    else
    {
        while (i < (10-len))
        {
            std::cout << " ";
            i++;
        }
        std::cout << str << "|";
    }
}

static void     display(Sample contact, int index)
{
    std::cout << "         " << index << "|";
    disp(contact.firstName);
    disp(contact.lastName);
    disp(contact.nickname);
    std::cout << std::endl;
}

static void     searcher(Sample contact[8],  int max)
{
    int     i(0);
	std::string		str;

    if (max == 0)
        std::cout << "Phone_book is empty" << std::endl;
    else
    {
        std::cout << "     index|first name| last name|  nickname|" << std::endl;
        while (i < max)
        {
            display(contact[i], (i+1));
            i++;
        }
        std::cout << "Choose index [1-8]: ";
		std::getline(std::cin, str);
        if (str[0] <= '0' || str[0] > '9' || str.length() != 1)
        {
            std::cout << "Incorrect choice" << std::endl;
        }
        else
		{
			i = atoi(&str[0]) - 1;
			if (i < max)
			{
				std::cout << "First Name: ";
				std::cout << contact[i].firstName << std::endl;
				std::cout << "Last Name: ";
				std::cout << contact[i].lastName << std::endl;
				std::cout << "Nickame: ";
				std::cout << contact[i].nickname << std::endl;
				std::cout << "Login: ";
				std::cout << contact[i].login << std::endl;
				std::cout << "Postal address: ";
				std::cout << contact[i].address << std::endl;
				std::cout << "Email address: ";
				std::cout << contact[i].email << std::endl;
				std::cout << "Phone number: ";
				std::cout << contact[i].phone << std::endl;
				std::cout << "Birthday date: ";
				std::cout << contact[i].birthday << std::endl;
				std::cout << "Favourite meal: ";
				std::cout << contact[i].meal << std::endl;
				std::cout << "Darkest secret: ";
				std::cout << contact[i].secret << std::endl;
			}
			else
            	std::cout << "Incorrect choice" << std::endl;
        }
    }
}

static int		check(int max, std::string cmd, Sample contact[8])
{
	if (EXIT)
		return (0);
	if (ADD)
    {
        if (max < 8)
        {
            std::cout << "Contact "<< max + 1 << std::endl;
            adder(contact[max]);
            max++;
        }
        else
            std::cout << "Too much contact" << std::endl;
    }
	if (SEARCH)
		searcher(contact, max);
    return (max);
}

int				main(void)
{
	std::string  cmd;
    Sample  contact[8];
    int     max = 0;

	while (1)
	{
		prompt();
		std::getline(std::cin, cmd);
		if (BAD_CMD)
			error(cmd);
		else
		{
		    max = check(max, cmd, contact);
            if (EXIT)
                return (0);
			cmd.clear();
		}
	}
	return (0);
}
