#include <iostream>
#include <ctime>
#include <cstdlib>

class  FragTrap
{
	public:

		FragTrap( void );
		FragTrap( int const n);
		FragTrap( std::string const name);
		FragTrap( FragTrap const & src );
		~FragTrap( void );

		FragTrap &	operator=( FragTrap const & rhs );

		int	getHitPoint ( void ) const;
		int	getMaxHitPoint ( void ) const;
		int	getEnergyPoint ( void ) const;
		int	getMaxEnergyPoint ( void ) const;
		int	getLevel ( void ) const;
		int	getMeleeAttDamage ( void ) const;
		int	getRangeAttDamage ( void ) const;
		int	getArmorDamageReduc ( void ) const;
		std::string		getName( void ) const;

		void			rangedAttack(std::string const & target);
		void			meleeAttack(std::string const & target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);
		void			vaulhunter_dot_exe(std::string const & target);

	private:

		std::string		_name;
		int				_hitPoints;
		int				_maxHitPoints;
		int				_energyPoints;
		int				_maxEnergyPoints;
		int				_level;
		int				_meleeAttDamage;
		int				_rangeAttDamage;
		int				_armorDamageReduc;
};
