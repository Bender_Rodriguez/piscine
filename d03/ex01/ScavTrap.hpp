/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 18:21:52 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 18:44:28 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <iostream>
#include <ctime>
#include <cstdlib>

class  ScavTrap
{
	public:

		ScavTrap( void );
		ScavTrap( int const n);
		ScavTrap( std::string const name);
		ScavTrap( ScavTrap const & src );
		~ScavTrap( void );

		ScavTrap &	operator=( ScavTrap const & rhs );

		int	getHitPoint ( void ) const;
		int	getMaxHitPoint ( void ) const;
		int	getEnergyPoint ( void ) const;
		int	getMaxEnergyPoint ( void ) const;
		int	getLevel ( void ) const;
		int	getMeleeAttDamage ( void ) const;
		int	getRangeAttDamage ( void ) const;
		int	getArmorDamageReduc ( void ) const;
		std::string		getName( void ) const;

		void			rangedAttack(std::string const & target);
		void			meleeAttack(std::string const & target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);
		void			challengeNewcomer(void) const;

	private:

		std::string		_name;
		int				_hitPoints;
		int				_maxHitPoints;
		int				_energyPoints;
		int				_maxEnergyPoints;
		int				_level;
		int				_meleeAttDamage;
		int				_rangeAttDamage;
		int				_armorDamageReduc;
};
