/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 14:22:13 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 18:47:24 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <ctime>
#include "FragTrap.hpp"

FragTrap::FragTrap( void ):
	_name("Unknow"),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Default Constructor called :";
	std::cout << "starting bootup sequence for " << this->_name << std::endl;
}

FragTrap::FragTrap( int const n ): _hitPoints(n) {}

FragTrap::FragTrap (std::string const name):
	_name(name),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Constructor with string parameter called :  ";
	std::cout << " starting bootup sequence for " << this->_name << std::endl;
}

FragTrap::FragTrap( FragTrap const & src )
{
	*this = src;
}

FragTrap::~FragTrap( void )
{
	std::cout << "Destructor called for " << this->_name << std::endl;
	std::cout << "FR4G-TP Autodestruction !" << std::endl;
}

FragTrap &	FragTrap::operator=( FragTrap const & rhs )
{
	if ( this != &rhs )
	{
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxHitPoints = rhs.getMaxHitPoint();
		this->_energyPoints = rhs.getEnergyPoint();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoint();
		this->_level = rhs.getLevel();
		this->_meleeAttDamage = rhs.getMeleeAttDamage();
		this->_rangeAttDamage = rhs.getRangeAttDamage();
		this->_armorDamageReduc = rhs.getArmorDamageReduc();
	}
		return *this;
}

int			FragTrap::getHitPoint (void) const
{
	return this->_hitPoints;
}

int			FragTrap::getMaxHitPoint (void) const
{
	return this->_maxHitPoints;
}

int			FragTrap::getEnergyPoint (void) const
{
	return this->_energyPoints;
}

int			FragTrap::getMaxEnergyPoint (void) const
{
	return this->_maxEnergyPoints;
}

int			FragTrap::getLevel (void) const
{
	return this->_level;
}

std::string	FragTrap::getName ( void ) const
{
	return this->_name;
}

void		FragTrap::rangedAttack(std::string const & target)
{
	std::cout << this->_name << " attacks " << target;
	std::cout << " at range, causing " << this->_rangeAttDamage;
	std::cout << " points of damage ! Does it taste good??" << std::endl;
}

void		FragTrap::meleeAttack(std::string const & target)
{
	std::cout << this->_name << " attacks " << target;
	std::cout << " at melee, causing " << this->_meleeAttDamage;
	std::cout << " points of damage ! taste my Shaolin monk style" << std::endl;
}

void		FragTrap::takeDamage(unsigned int amount)
{
	int		d;
	if (this->_armorDamageReduc >= amount)
	{
		std::cout << "I did not feel nothing you fool!! ";
		std::cout << "(blocked by armor damage reduction)" << std::endl;
		return ;
	}
	d = amount - this->_armorDamageReduc;
	if (this->_hitPoints <= d)
		this->_hitPoints = 0;
	else
		this->_hitPoints -= d;
	std::cout << this->_name << " undergoes " << d << "point(s) of damages";
	std::cout << std::endl << "Outch... thath's hurt !" << std::endl;
}

void		FragTrap::beRepaired(unsigned int amount)
{
	int		heal;
	if (this->_hitPoints + amount >= this->_maxHitPoints)
	{
		heal = this->_maxHitPoints - this->_hitPoints;
		this->_hitPoints = this->_maxHitPoints;
	}
	else
	{
		heal = amount;
		this->_hitPoints += amount;
	}
	std::cout << this->_name << " have been healed by " << heal << " HP(s)";
	std::cout << std::endl << "I feel the life growwing in me..." << std::endl;
}

void		FragTrap::vaulhunter_dot_exe(std::string const & target)
{
	std::string	attack[5] = {"Panzer Dragon kick!", "Germaaaaan Suuuuuuplex!!",
								"Ippon!", "HIAKUSHIKI CANNON! (Netero style)",
									"Spinning bird Kick! (Chun Li style)" };
	int			r = rand() % 5;
	if (this->_energyPoints < 25)
	{
		std::cout << "Not enough energy for Vaulhunter !" << std::endl;
		return ;
	}
	std::cout << this->_name << "launch his MANGA attack on " << target;
	std::cout << ": " << attack[r] << std::endl;
	std::cout << target << " looses 45 HP(s)" << std::endl;
	this->_energyPoints -= 25;
}

int			FragTrap::getMeleeAttDamage (void) const
{
	return this->_meleeAttDamage;
}

int			FragTrap::getRangeAttDamage (void) const
{
	return this->_rangeAttDamage;
}

int			FragTrap::getArmorDamageReduc (void) const
{
	return this->_armorDamageReduc;
}
