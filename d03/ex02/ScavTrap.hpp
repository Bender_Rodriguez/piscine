/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 18:21:52 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 19:28:19 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "ClapTrap.hpp"

class  ScavTrap:public ClapTrap
{
	public:

		ScavTrap( void );
		ScavTrap( int const n);
		ScavTrap( std::string const name);
		ScavTrap( ScavTrap const & src );
		~ScavTrap( void );

		ScavTrap &	operator=( ScavTrap const & rhs );

		void			rangedAttack(std::string const & target);
		void			meleeAttack(std::string const & target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);
		void			challengeNewcomer(void) const;
};

#endif
