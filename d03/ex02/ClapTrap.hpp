/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 18:56:16 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 19:27:44 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <iostream>
#include <ctime>
#include <cstdlib>

class  ClapTrap
{
	public:

		ClapTrap( void );
		ClapTrap( int const n);
		ClapTrap( std::string const name);
		ClapTrap( ClapTrap const & src );
		~ClapTrap( void );

		ClapTrap &	operator=( ClapTrap const & rhs );

		int	getHitPoint ( void ) const;
		int	getMaxHitPoint ( void ) const;
		int	getEnergyPoint ( void ) const;
		int	getMaxEnergyPoint ( void ) const;
		int	getLevel ( void ) const;
		int	getMeleeAttDamage ( void ) const;
		int	getRangeAttDamage ( void ) const;
		int	getArmorDamageReduc ( void ) const;
		std::string		getName( void ) const;

		void			rangedAttack(std::string const & target);
		void			meleeAttack(std::string const & target);
		void			takeDamage(unsigned int amount);
		void			beRepaired(unsigned int amount);

	protected:

		std::string		_name;
		int				_hitPoints;
		int				_maxHitPoints;
		int				_energyPoints;
		int				_maxEnergyPoints;
		int				_level;
		int				_meleeAttDamage;
		int				_rangeAttDamage;
		int				_armorDamageReduc;
};

#endif
