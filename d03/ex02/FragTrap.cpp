/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 14:22:13 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 19:24:13 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <ctime>
#include "FragTrap.hpp"
#include "ClapTrap.hpp"

FragTrap::FragTrap( void ):
	_name = "FR4G",
	_hitPoints = 100,
	_maxHitPoints = 100,
	_energyPoints = 100,
	_maxEnergyPoints = 100,
	_level = 1,
	_meleeAttDamage = 30,
	_rangeAttDamage = 20,
	_armorDamageReduc = 5
{
	std::cout << "Default Constructor called :";
	std::cout << "starting bootup sequence for " << this->_name << std::endl;
}

FragTrap::FragTrap( int const n ): _hitPoints(n) {}

FragTrap::FragTrap (std::string const name):
	_name(name),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Constructor with string parameter called :  ";
	std::cout << " starting bootup sequence for " << this->_name << std::endl;
}

FragTrap::FragTrap( FragTrap const & src )
{
	*this = src;
}

FragTrap::~FragTrap( void )
{
	std::cout << "Destructor called for " << this->_name << std::endl;
	std::cout << "FR4G-TP Autodestruction !" << std::endl;
}

FragTrap &	FragTrap::operator=( FragTrap const & rhs )
{
	if ( this != &rhs )
	{
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxHitPoints = rhs.getMaxHitPoint();
		this->_energyPoints = rhs.getEnergyPoint();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoint();
		this->_level = rhs.getLevel();
		this->_meleeAttDamage = rhs.getMeleeAttDamage();
		this->_rangeAttDamage = rhs.getRangeAttDamage();
		this->_armorDamageReduc = rhs.getArmorDamageReduc();
	}
		return *this;
}

int			FragTrap::getHitPoint (void) const
{
	return this->_hitPoints;
}

int			FragTrap::getMaxHitPoint (void) const
{
	return this->_maxHitPoints;
}

int			FragTrap::getEnergyPoint (void) const
{
	return this->_energyPoints;
}

int			FragTrap::getMaxEnergyPoint (void) const
{
	return this->_maxEnergyPoints;
}

int			FragTrap::getLevel (void) const
{
	return this->_level;
}

std::string	FragTrap::getName ( void ) const
{
	return this->_name;
}

void		FragTrap::rangedAttack(std::string const & target)
{
	ClapTrap::rangeAttack(target);
}

void		FragTrap::meleeAttack(std::string const & target)
{
	ClapTrap::meleeAttack(target);
}

void		FragTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
}

void		FragTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
}

void		FragTrap::vaulhunter_dot_exe(std::string const & target)
{
	std::string	attack[5] = {"Panzer Dragon kick!", "Germaaaaan Suuuuuuplex!!",
								"Ippon!", "HIAKUSHIKI CANNON! (Netero style)",
									"Spinning bird Kick! (Chun Li style)" };
	int			r = rand() % 5;
	if (this->_energyPoints < 25)
	{
		std::cout << "Not enough energy for Vaulhunter !" << std::endl;
		return ;
	}
	std::cout << this->_name << "launch his MANGA attack on " << target;
	std::cout << ": " << attack[r] << std::endl;
	std::cout << target << " looses 45 HP(s)" << std::endl;
	this->_energyPoints -= 25;
}

int			FragTrap::getMeleeAttDamage (void) const
{
	return this->_meleeAttDamage;
}

int			FragTrap::getRangeAttDamage (void) const
{
	return this->_rangeAttDamage;
}

int			FragTrap::getArmorDamageReduc (void) const
{
	return this->_armorDamageReduc;
}
