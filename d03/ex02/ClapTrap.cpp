/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 18:55:21 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 18:57:28 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <iostream>
#include <ctime>
#include "ClapTrap.hpp"

ClapTrap::ClapTrap( void ):
	_name("Unknow"),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Default Constructor called for: " << this->_name << std::endl;
}

ClapTrap::ClapTrap( int const n ): _hitPoints(n) {}

ClapTrap::ClapTrap (std::string const name):
	_name(name),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Constructor with string parameter called for: " << this->_name << std::endl;
}

ClapTrap::ClapTrap( ClapTrap const & src )
{
	*this = src;
}

ClapTrap::~ClapTrap( void )
{
	std::cout << "Destructor called for " << this->_name << std::endl;
	std::cout << "ByeBye !" << std::endl;
}

ClapTrap &	ClapTrap::operator=( ClapTrap const & rhs )
{
	if ( this != &rhs )
	{
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxHitPoints = rhs.getMaxHitPoint();
		this->_energyPoints = rhs.getEnergyPoint();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoint();
		this->_level = rhs.getLevel();
		this->_meleeAttDamage = rhs.getMeleeAttDamage();
		this->_rangeAttDamage = rhs.getRangeAttDamage();
		this->_armorDamageReduc = rhs.getArmorDamageReduc();
	}
		return *this;
}

int			ClapTrap::getHitPoint (void) const
{
	return this->_hitPoints;
}

int			ClapTrap::getMaxHitPoint (void) const
{
	return this->_maxHitPoints;
}

int			ClapTrap::getEnergyPoint (void) const
{
	return this->_energyPoints;
}

int			ClapTrap::getMaxEnergyPoint (void) const
{
	return this->_maxEnergyPoints;
}

int			ClapTrap::getLevel (void) const
{
	return this->_level;
}

std::string	ClapTrap::getName ( void ) const
{
	return this->_name;
}

void		ClapTrap::rangedAttack(std::string const & target)
{
	std::cout << this->_name << " attacks " << target;
	std::cout << " at range, causing " << this->_rangeAttDamage;
	std::cout << " points of damage ! Does it taste good??" << std::endl;
}

void		ClapTrap::meleeAttack(std::string const & target)
{
	std::cout << this->_name << " attacks " << target;
	std::cout << " at melee, causing " << this->_meleeAttDamage;
	std::cout << " points of damage ! taste my Shaolin monk style" << std::endl;
}

void		ClapTrap::takeDamage(unsigned int amount)
{
	int		d;
	if (this->_armorDamageReduc >= amount)
	{
		std::cout << "I did not feel nothing you fool!! ";
		std::cout << "(blocked by armor damage reduction)" << std::endl;
		return ;
	}
	d = amount - this->_armorDamageReduc;
	if (this->_hitPoints <= d)
		this->_hitPoints = 0;
	else
		this->_hitPoints -= d;
	std::cout << this->_name << " undergoes " << d << "point(s) of damages";
	std::cout << std::endl << "Outch... thath's hurt !" << std::endl;
}

void		ClapTrap::beRepaired(unsigned int amount)
{
	int		heal;
	if (this->_hitPoints + amount >= this->_maxHitPoints)
	{
		heal = this->_maxHitPoints - this->_hitPoints;
		this->_hitPoints = this->_maxHitPoints;
	}
	else
	{
		heal = amount;
		this->_hitPoints += amount;
	}
	std::cout << this->_name << " have been healed by " << heal << " HP(s)";
	std::cout << std::endl << "I feel the life growwing in me..." << std::endl;
}

int			ClapTrap::getMeleeAttDamage (void) const
{
	return this->_meleeAttDamage;
}

int			ClapTrap::getRangeAttDamage (void) const
{
	return this->_rangeAttDamage;
}

int			ClapTrap::getArmorDamageReduc (void) const
{
	return this->_armorDamageReduc;
}
