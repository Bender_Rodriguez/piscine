/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 14:25:15 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 19:29:51 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

void		getInfo(FragTrap & F)
{
	std::string		name = F.getName();
	std::string		status = F.getHitPoint() == 0 ? "DeaD" : "AlivE";

	std::cout << std::endl << name << " is " << status << "," << std::endl;
	std::cout << "He has " << F.getHitPoint() << " HP(s) and ";
	std::cout << F.getEnergyPoint() << " Energy point(s)." << std::endl;
}

int			main(void)
{
	FragTrap		bot("General Franky");
	ScavTrap		one("Scav");

	one.challengeNewcomer();
	one.challengeNewcomer();
	one.challengeNewcomer();
	one.challengeNewcomer();
	one.challengeNewcomer();
	getInfo(bot);
	bot.rangedAttack("Mingo");
	bot.meleeAttack("Senior Pink");
	getInfo(bot);
	bot.takeDamage(40);
	bot.takeDamage(28);
	getInfo(bot);
	bot.beRepaired(24);
	getInfo(bot);
	bot.beRepaired(666);
	getInfo(bot);
	bot.vaulhunter_dot_exe("Bartolomeo");
	getInfo(bot);
	bot.vaulhunter_dot_exe("Dr. Vegapunk");
	bot.vaulhunter_dot_exe("Shiro Hige");
	bot.vaulhunter_dot_exe("Cavendish");
	getInfo(bot);
	bot.vaulhunter_dot_exe("Cavendish");
	bot.takeDamage(200);
	getInfo(bot);
	return (0);
}
