/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/05 18:21:58 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/05 19:25:55 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <ctime>
#include "ScavTrap.hpp"
#include "ClapTrap.hpp"

ScavTrap::ScavTrap( void ):
	_name = "Sc4v",
	_hitPoints = 100,
	_maxHitPoints = 100,
	_energyPoints = 100,
	_maxEnergyPoints = 100,
	_level = 1,
	_meleeAttDamage = 30,
	_rangeAttDamage = 20,
	_armorDamageReduc = 5
{
	std::cout << "Default Constructor called for: " << this->_name << std::endl;
}

ScavTrap::ScavTrap( int const n ): _hitPoints(n) {}

ScavTrap::ScavTrap (std::string const name):
	_name(name),
	_hitPoints(100),
	_maxHitPoints(100),
	_energyPoints(100),
	_maxEnergyPoints(100),
	_level(1),
	_meleeAttDamage(30),
	_rangeAttDamage(20),
	_armorDamageReduc(5)
{
	std::cout << "Constructor with string parameter called for: " << this->_name << std::endl;
}

ScavTrap::ScavTrap( ScavTrap const & src )
{
	*this = src;
}

ScavTrap::~ScavTrap( void )
{
	std::cout << "Destructor called for " << this->_name << std::endl;
	std::cout << "ByeBye !" << std::endl;
}

ScavTrap &	ScavTrap::operator=( ScavTrap const & rhs )
{
	if ( this != &rhs )
	{
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxHitPoints = rhs.getMaxHitPoint();
		this->_energyPoints = rhs.getEnergyPoint();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoint();
		this->_level = rhs.getLevel();
		this->_meleeAttDamage = rhs.getMeleeAttDamage();
		this->_rangeAttDamage = rhs.getRangeAttDamage();
		this->_armorDamageReduc = rhs.getArmorDamageReduc();
	}
		return *this;
}

int			ScavTrap::getHitPoint (void) const
{
	return this->_hitPoints;
}

int			ScavTrap::getMaxHitPoint (void) const
{
	return this->_maxHitPoints;
}

int			ScavTrap::getEnergyPoint (void) const
{
	return this->_energyPoints;
}

int			ScavTrap::getMaxEnergyPoint (void) const
{
	return this->_maxEnergyPoints;
}

int			ScavTrap::getLevel (void) const
{
	return this->_level;
}

std::string	ScavTrap::getName ( void ) const
{
	return this->_name;
}

void		ScavTrap::rangedAttack(std::string const & target)
{
	ClapTrap::rangeAttack(target);
}

void		ScavTrap::meleeAttack(std::string const & target)
{
	ClapTrap::meleeAttack(target);
}

void		ScavTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
}

void		ScavTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
}

void		ScavTrap::challengeNewcomer(void) const
{
	std::string	punchLine[5] = {"I Dare YOU to fight ME!",
		"You are NOT PREPARDED!","Koii!", "You'll NEVER defeat me!",
													"I'm here to kill you" };
	int			r = rand() % 5;
	std::cout << punchLine[r] << std::endl;
}

int			ScavTrap::getMeleeAttDamage (void) const
{
	return this->_meleeAttDamage;
}

int			ScavTrap::getRangeAttDamage (void) const
{
	return this->_rangeAttDamage;
}

int			ScavTrap::getArmorDamageReduc (void) const
{
	return this->_armorDamageReduc;
}
