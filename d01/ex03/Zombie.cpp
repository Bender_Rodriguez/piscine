/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 15:16:06 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 15:23:51 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include <iostream>

Zombie::Zombie(std::string name, std::string type): _type(type), _name(name) 
{}

Zombie::Zombie(void): _type("Unknow"), _name("Unknow") {}

Zombie::~Zombie(){}

void		Zombie::announce(void)
{
	std::cout << "The " << this->_type << " Zombie " << this->_name;
	std::cout << " in da place!" << std::endl;
}

void		Zombie::setName(std::string name, std::string type)
{
	this->_name = name;
	this->_type = type;
}
