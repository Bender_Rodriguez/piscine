/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 17:10:24 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 17:17:25 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieHorde.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>

int				main(void)
{
	ZombieHorde		hord(19);

	hord.announce();
	std::cout << "LOL" << std::endl;
	hord.announce();
	return (0);
}
