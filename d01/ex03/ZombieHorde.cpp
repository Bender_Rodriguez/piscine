/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 16:42:01 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 15:21:59 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.hpp"
#include <iostream>
#include <ctime>
#include <cstdlib>

ZombieHorde::ZombieHorde(int N): _nbr(N)
{
	int				i;

	std::string 	tab[8] = {"John", "Rambo", "Glenn", "Frog", "Thanatos", "Bouldacier", "Jet Li", "Bruce Lee"};
	std::string 	typeTab[8] = {"Blue", "Green", "Longo", "Arch", "Badmotos", "Unsnatcher", "Turbo", "Ninja"};
	this->_horde = new Zombie[N];
	for (int o=0; o<N; o++)
	{
		i = rand() % 8;
		this->_horde[o].setName(tab[i], typeTab[i]);
	}
	
}

ZombieHorde::~ZombieHorde(void)
{
	delete [] this->_horde;
}

void			ZombieHorde::announce(void)
{
	int			i = this->_nbr - 1;

	while (i >= 0)
	{
		this->_horde[i].announce();
		i--;
	}
}

