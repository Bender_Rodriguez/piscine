/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 15:20:14 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 15:24:23 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

#include <iostream>

class Zombie
{
	public:

	Zombie(std::string type, std::string name);
	Zombie(void);
	~Zombie(void);

	void		announce(void);
	void		setName(std::string name, std::string type);

	private:
	std::string		_type;
	std::string		_name;

};

#endif
