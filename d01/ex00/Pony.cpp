/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 12:38:41 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 13:45:06 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "Pony.hpp"

Pony::Pony(std::string s1, std::string s2): color(s1), flapiness(s2)
{
	std::cout << "A little " << color << " and " << flapiness;
	std::cout << " powny have been created" << std::endl;
}
Pony::~Pony()
{
	std::cout << "The pony have been killed\n" << std::endl;
}
