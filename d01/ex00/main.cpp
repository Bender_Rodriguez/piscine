/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 12:50:30 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 13:48:22 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void			ponyOnTheHeap(std::string color, std::string flapiness)
{
	Pony		*ponyOnTheHeap = new Pony(color,flapiness);
	
	std::cout << "The pony is on the Heap, yeah men.." << std::endl;
	delete ponyOnTheHeap;
}

void			ponyOnTheStack(std::string color, std::string flapiness)
{
	Pony		ponyOnTheStack(color, flapiness);
	std::cout << "The pony is on the Stack, crap.." << std::endl;
}

int				main(void)
{
	ponyOnTheHeap("red", "not so flappy");
	ponyOnTheStack("rainbow", "flappy");
	
	return (0);
}
