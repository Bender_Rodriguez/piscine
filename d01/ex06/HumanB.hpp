/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 16:34:28 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 18:24:33 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HUMANB_HPP
# define HUMANB_HPP

#include <iostream>
#include "Weapon.hpp"

class HumanB
{
	public:

		void		setWeapon(Weapon &gun);
		void		attack();

		HumanB(std::string);
		~HumanB();

	private:

		std::string	_name;
		Weapon		*_weapon;
};


#endif
