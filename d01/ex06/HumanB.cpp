/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 16:33:23 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 18:24:35 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.hpp"

HumanB::HumanB(std::string name): _name(name) {}

HumanB::~HumanB(void) {}

void	HumanB::setWeapon(Weapon &gun)
{
	this->_weapon = &gun;
}

void	HumanB::attack(void)
{
	std::cout << this->_name << " attacks with ";
	std::cout << this->_weapon->getType() << std::endl;
}
