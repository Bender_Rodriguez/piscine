/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 17:18:51 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 17:25:23 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int			main(void)
{
	std::string		s("HI THIS IS BRAIN");
	std::string		*str;
	std::string		&lol = s;

	str = &s;
	std::cout << *str << std::endl;
	std::cout << lol << std::endl;
	return (0);
}
