/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Replace.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 19:24:20 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 22:55:47 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REPLACE_HPP
# define REPLACE_HPP

#include <iostream>
#include <fstream>

class Ftsed
{
	public:


		Ftsed(std::string name, std::string s1, std::string s2);
		~Ftsed();

		std::string			_name;
		std::string			_s1;
		std::string			_s2;
};

#endif
