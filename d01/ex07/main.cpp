/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 22:42:37 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 23:06:51 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
/*
#include "Replace.hpp"

*/
/*
file,  newFile,  strFind, strReplace
	(av[1], newFileName, s1, s2)
												line = temp

void			ftseder(char **av)
{

	Ftsed				saver = Ftsed(av[1], av[2], av[3]);
	std::ifstream 		inputFile(saver._name, std::ios::in);
	std::string			newFileName = saver._name + ".replace";
	std::ofstream 		outputFile(newFileName.c_str());
	std::string			temp;
	int					len;

	while (std::getline(inputFile, temp))
	{
		len = 0;
		while ((len = temp.find(saver._s1, len)) != std::string::npos)
		{
			temp.replace(len, saver._s1.size(), saver._s2);
			len += saver._s1.size();
		}
		outputFile << temp << std::endl;
	}
}
												*/

bool			checker(std::string name)
{
	std::ifstream		fileName(name);
	if (fileName)
		return true;
	else
		return false;
}

int				main(int ac, char **av)
{
	if (ac != 4)
	{
		std::cout << "Invalid number of arguments" << std::endl;
		return (1);
	}
	else
	{
		if (checker(av[1]))
		{
			std::string			tab[3] = {av[1], av[2], av[3]};
			std::ifstream 		inputFile(tab[0], std::ios::in);
			std::string			newFileName = tab[0] + ".replace";
			std::ofstream 		outputFile(newFileName.c_str());
			std::string			temp;
			size_t				len;

			while (std::getline(inputFile, temp))
			{
				len = 0;
				while ((len = temp.find(tab[1], len)) != std::string::npos)
				{
					temp.replace(len, tab[1].size(), tab[2]);
					len += tab[1].size();
				}
				outputFile << temp << std::endl;
			}
		}
		//	ftseder(av);
		else
			std::cout << "Replace: " << av[1] << ": No such file" << std::endl;
	}
	return (0);
	/*
	if (fileName == "" || s1 == "")
		return (std::cout << "Error : Arguement empty" << std::endl, 1);
		*/
}
