/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Replace.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 19:56:16 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 22:55:49 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Replace.hpp"

Ftsed::Ftsed(std::string name, std::string s1, std::string s2):
	_name(name),
	_s1(s1),
	_s2(s2) {}

Ftsed::~Ftsed() {}
