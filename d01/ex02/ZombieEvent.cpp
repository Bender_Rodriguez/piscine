/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 15:46:08 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 16:34:55 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.hpp"
#include <iostream>

ZombieEvent::ZombieEvent(void)
{
	this->_type = "Deadman";
}
ZombieEvent::~ZombieEvent(void){}

void		ZombieEvent::setZombieType(std::string type)
{
	this->_type = type;
}

Zombie		*ZombieEvent::newZombie(std::string name)
{
	return (new Zombie(name, this->_type));
}

void		ZombieEvent::randomChump(void)
{
	int				i = rand() % 8;
	Zombie			*zombar;
	std::string 	tab[8] = {"John", "Rambo", "Glenn", "Frog", "Thanatos", "Bouldacier", "Jet Li", "Bruce Lee"};
	setZombieType("Dead");
	zombar = newZombie(tab[i]);
	zombar->announce();
	delete zombar;
}
