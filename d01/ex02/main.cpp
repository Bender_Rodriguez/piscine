/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 16:08:22 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 16:39:31 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"
#include <cstdlib>
#include <ctime>

int			main(void)
{
	ZombieEvent		zoum, zoom;
	Zombie			Z("BOB", "FAT");

	srand(time(NULL));
	zoum.randomChump();
	Z.announce();
	zoom.randomChump();
	return (0);
}
