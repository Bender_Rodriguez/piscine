/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/03 15:50:21 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/03 16:35:06 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIEEVENT_HPP
# define ZOMBIEEVENT_HPP

#include <iostream>
#include "Zombie.hpp"

class ZombieEvent
{
	public:

	ZombieEvent(void);
	~ZombieEvent(void);

	void		setZombieType(std::string type);
	Zombie		*newZombie(std::string name);
	void		randomChump(void);

	private:
	std::string		_type;

};

#endif
