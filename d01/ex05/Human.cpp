/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 12:50:49 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 15:41:28 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"

Human::Human(void)
{
	this->_brain = new Brain();
}

Human::~Human(void)
{
	delete this->_brain;
}

std::string	Human::identify(void) const
{
	return (this->_brain->identify());
}

const Brain	&Human::getBrain(void)
{
	return (*(this->_brain));
}
