/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yaitalla <yaitalla@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/04 12:46:50 by yaitalla          #+#    #+#             */
/*   Updated: 2015/11/04 16:00:27 by yaitalla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain(void){}

Brain::~Brain(void){}

std::string		Brain::identify(void) const
{
	std::string			str;
	std::stringstream	s;
	long				l = (long)this;
	long				p;

	p = l;
	s << "0x";
	s << std::hex;
	s << std::uppercase;
	s << l << std::endl;
	s >> str;
	return (str);
}
